---
title: "About me"
date: 2022-04-30
tags: ["about", "me"]
author: "enric_0r"
---

# **Welcome everyone!** 👋
#### I'm Enrico and I work as a **Security Engineer**@[Sysdig](https://sysdig.com/).

- ## **Why this blog?**
    - I'm creating this blog to share my knowledge, experiences, tips and tricks.

- ## **A little about my career**
    - Started as a SOC Analyst / Security Engineer. During this experience I have analyzed security incidents and helped improving the overall security posture of customers. I developed also specific scripts to improve and ease the workflow. Then, I wanted to change side and to assess from a red team perspective security vulnerabilities. If you want to know more about my career you can always visit my [LinkedIn profile](https://www.linkedin.com/in/enric0r/).

- ## **A little about myself**
    - I like music 🎵, videogames 🕹️, security 🔒 and hacking 💻. I tend to think outside the "box" and break everything I can to gain knowledge. Curiosity (**not the NASA rover**) is what helps me learn new things everyday. Software development and automation are also in my interests.

- ## Support me
    - If you might want to support me :P (appreciate it)
    [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Z8Z0CKV3L)